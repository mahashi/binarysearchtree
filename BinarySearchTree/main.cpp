#include <iostream>
#include "BinarySearchTree.h"
#include <vector>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <climits>

#define N INT16_MAX*24

struct cmp {
	bool operator()(const int &lhs, const int &rhs)
	{
		return lhs < rhs;
	}
};

int main()
{
	BST::BinarySearchTree<int, cmp> bst;
	std::vector<int> vec;

	std::cout << "N = " << N << std::endl;


	//Tworzenie wektora z losowymi warto�ciami
	for (int i = 0; i < N; ++i)
		vec.push_back(i);

	//pomieszanie wektora
	std::random_shuffle(vec.begin(), vec.end());

	std::chrono::high_resolution_clock::time_point clk1 = std::chrono::high_resolution_clock::now();
	//wstawianie wszystkich element�w wektora do drzewa
	for (int ele : vec)
	{
		//std::cout << "ele: " << ele << std::endl;
		if (!bst.insert(ele))
			std::cout << "Couldn't add: " << ele << std::endl;
	}
	std::chrono::high_resolution_clock::time_point clk2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(clk2 - clk1);
	std::cout << "wstawianie wektora " << N << " elementowego do drzewa: " << time_span.count() << " sekund.\n";

	//jeszcze raz mieszamy aby wyszkuka� w innej kolejno�ci
	std::random_shuffle(vec.begin(), vec.end());

	clk1 = std::chrono::high_resolution_clock::now();
	for (int ele : vec)
	{
		if (!bst.find(ele))
			std::cout << "Could't find: " << ele << std::endl;
	}
	clk2 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(clk2 - clk1);
	std::cout << "wyszukiwanie elemet�w wektora " << N << " elementowego w drzewie: " << time_span.count() << " sekund.\n";

	clk1 = std::chrono::high_resolution_clock::now();

	std::cout << "Najmniejszy element: " << bst.min() << std::endl;

	clk2 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(clk2 - clk1);
	std::cout << "wyszukiwanie najmniejszego elementu " << N << " elementowego drzewa: " << time_span.count() << " sekund.\n";

	clk1 = std::chrono::high_resolution_clock::now();

	std::cout << "Najwiekszy element: " << bst.max() << std::endl;

	clk2 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(clk2 - clk1);
	std::cout << "wyszukiwanie najwi�kszego elementu " << N << " elementowego drzewa: " << time_span.count() << " sekund.\n";

	//jeszcze raz mieszamy, �eby usuna� w innej kolejno�ci
	std::random_shuffle(vec.begin(), vec.end());

	clk1 = std::chrono::high_resolution_clock::now();
	for (int ele : vec)
	{
		if (!bst.erase(ele))
			std::cout << "Could't erase: " << ele << std::endl;
	}
	clk2 = std::chrono::high_resolution_clock::now();
	time_span = std::chrono::duration_cast<std::chrono::duration<double>>(clk2 - clk1);
	std::cout << "usuwanie wektora " << N << " elementowego z drzewa: " << time_span.count() << " sekund.\n";

	return 0;
}

