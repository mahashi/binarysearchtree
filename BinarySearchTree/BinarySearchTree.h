#pragma once
#include "Node.h"
#include <functional>

namespace BST {

	template <typename T, typename Compare = std::less<T>>
	class BinarySearchTree
	{
	private:
		//typedef of shared_ptr type
		using node_sh = std::shared_ptr<Node<T, Compare>>;

		node_sh root; //root of tree
	public:

		BinarySearchTree()
		{

		}

		T& min()
		{
			return min(root);
		}

		T& min(node_sh subtree)
		{
			node_sh current = subtree;
			if (!current)
				throw 1; // if there is nothing in the subtree, temp solution

						 //for minimum we go left until the end
			while (current->left)
				current = current->left;

			return current->value;
		}

		T& max()
		{
			return max(root);
		}

		T& max(node_sh subtree)
		{
			node_sh current = subtree;
			if (!current)
				throw 1; // if there is nothing in the subtree, temp solution

						 //for minimum we go left until the end
			while (current->right)
				current = current->right;

			return current->value;
		}


		bool insert(T x)
		{
			node_sh current = root;
			node_sh next;

			if (!root) {
				//if root is empty we create new root
				root.reset(new Node<T, Compare>(x));
				return true;
			}

			for (;;)
			{
				//if the value we want to add is "lower" than current node
				if (x < *current)
				{
					//if left child exists we change current node
					if (current->left)
						current = current->left;
					else
					{
						//if left child doesn't exist we create new node with x value
						current->left.reset(new Node<T, Compare>(x, current));
						return true;
					}

				}
				//if the value we want to add is "greater" than current node
				else if (x > *current)
				{
					//if right child exists we change current node
					if (current->right)
						current = current->right;
					else
					{
						//if right child doesn't exist we create new node with x value
						current->right.reset(new Node<T, Compare>(x, current));
						return true;
					}
				}
				else
					//returns false if the value is already in tree
					return false;
			}
		}

		bool erase(T x)
		{
			return erase(x, root);
		}

		bool erase(T x, node_sh subtree)
		{
			//if subtree is null return false
			if (!subtree)
				return false;

			if (x < *subtree)
				//if value we want to erase is "lower"
				//we recursively go down to left child
				return this->erase(x, subtree->left);

			if (x > *subtree)
				//if value we want to erase is "greater"
				//we recursively go down to right child
				return this->erase(x, subtree->right);


			if (subtree->left && subtree->right)
			{
				//in this case children aren't nulls
				//so we search for successor
				T temp = this->min(subtree->right);
				this->erase(temp, subtree->right);
				subtree->value = temp;

				return true;
			}
			else if (!subtree->left && !subtree->right)
			{
				//both children are nulls

				//if root we erase it
				if (!subtree->parent)
				{
					root.reset();
					return true;
				}

				//if not root, current node is leaf so we delete if from parent
				if (*subtree < *subtree->parent)
					subtree->parent->left.reset();
				else
					subtree->parent->right.reset();

				return true;
			}
			else
			{
				//in this case one of the children is null
				//-------------------------
				//if there is no parent so we need to erase root
				//if left child is null we insert right child as root
				//in the other case left child
				if (!subtree->parent)
				{
					root = (!subtree->left) ? subtree->right : subtree->left;
					return true;
				}

				//if we don't neet to erase root
				//-----------------------
				//if left child is null
				if (!subtree->left)
				{
					//we check which child we need to erase
					if (*subtree < *subtree->parent)
					{
						subtree->parent->left = subtree->right;
						subtree->right->parent = subtree->parent;
					}
					else
					{
						subtree->parent->right = subtree->right;
						subtree->right->parent = subtree->parent;
					}

				}
				else
					//if right child is null
				{
					//we check which child we need to erase
					if (*subtree < *subtree->parent)
					{
						subtree->parent->left = subtree->left;
						subtree->left->parent = subtree->parent;
					}
					else
					{
						subtree->parent->right = subtree->left;
						subtree->left->parent = subtree->parent;
					}
				}
				return true;
			}
		}

		bool find(T x)
		{
			node_sh current = root;

			//we run over the tree until node is equal to x
			//if we don't find equal value, return null
			while (current)
			{
				if (x > *current)
					current = current->right;
				else if (x < *current)
					current = current->left;
				else
					return true;
			}

			return false;
		}
	};
}

